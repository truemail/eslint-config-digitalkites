# eslint-config-digitalkites
This package provides a ESLint shareable config for the DigitalKites Company's JS coding style.

## Why another config?
To maintain our own config.

## Installation
npm i eslint eslint-config-digitalkites --save-dev

## Usage
Create a .eslintrc.js config file:

### .eslintrc.js

```js
module.exports = {
    "extends": "eslint-config-digitalkites"
}

```
## For Angular
For Angular Projects you can use the eslint-config-digitalkites/angular config:

```js
module.exports = {
    "extends": "eslint-config-digitalkites/angular"
}