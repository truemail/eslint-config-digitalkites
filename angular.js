module.exports = {
    extends: [
        './index.js'
    ].map(require.resolve),
    env: {
        browser: true,
        es6: true,
    },
    rules: {

    },
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        allowImportExportEverywhere: true
    },
    plugins: [
        "@typescript-eslint"
    ],
    parser: "@typescript-eslint/parser"

}
