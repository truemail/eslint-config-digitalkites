module.exports = {
    globals: {},
    extends: "eslint:recommended",
    overrides: [
        {
            files: ["src/*.ts", "app/*.js"],
            rules: {
                "no-async-promise-executor": "off",
                "camelcase": [
                    "error"
                ],
                "indent": [
                    "error",
                    2
                ],
                "max-depth": [
                    "error",
                    {
                        "max": 4
                    }
                ],
                "max-len": [
                    "error",
                    {
                        "code": 80,
                        "tabWidth": 4,
                        "comments": 80,
                        "ignorePattern": "true",
                        "ignoreComments": true,
                        "ignoreUrls": true,
                        "ignoreStrings": true,
                        "ignoreTemplateLiterals": true,
                        "ignoreRegExpLiterals": true
                    }
                ],
                "max-lines": [
                    "error",
                    {
                        "max": 300,
                        "skipBlankLines": true,
                        "skipComments": true
                    }
                ],
                "max-nested-callbacks": [
                    "error",
                    {
                        "max": 3
                    }
                ],
                // "consistent-return": [
                //     "error", { "treatUndefinedAsUnspecified": false }
                // ],
                "eqeqeq": [
                    "error",
                    "always"
                ],
                "brace-style": [
                    "error",
                    "1tbs",
                    {
                        "allowSingleLine": true
                    }
                ],
                "no-eval": [
                    "error",
                    {
                        "allowIndirect": true
                    }
                ],
                "no-implied-eval": [
                    "error"
                ],
                "quotes": [
                    "error",
                    "single",
                    {
                        "avoidEscape": true,
                        "allowTemplateLiterals": true
                    }
                ],
                "no-mixed-operators": [
                    "error",
                    {
                        "groups": [
                            ["+", "-", "*", "/", "%", "**"],
                            ["&", "|", "^", "~", "<<", ">>", ">>>"],
                            ["==", "!=", "===", "!==", ">", ">=", "<", "<="],
                            ["&&", "||"],
                            ["in", "instanceof"]
                        ],
                        "allowSamePrecedence": true
                    }
                ],
                "global-require": [
                    "error"
                ],
                "handle-callback-err": [
                    "error",
                    "^.+Error$"
                ],
                "callback-return": [
                    "error"
                ],
                "one-var-declaration-per-line": [
                    "error",
                    "always"
                ],
                "semi": [
                    "error",
                    "always"
                ]
            }
        }
    ],
    "plugins": [
        "node"
    ],
    "parserOptions": {
        "ecmaVersion": 8,
        sourceType: "module"
        
    },
    "env": {
        "es6": true,
        "node": true,
        "browser": false
    }
};
